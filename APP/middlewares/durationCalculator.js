const findGame   = require('../services/findGame');
const updateGame = require('../services/updateGame');
const GAME_STATE = require('../libs/gameStates.json');

function calculate (req, res, next) {
  return findGame.findAllGames({})
    .then((games) => {
      let promises = [];
      for (let i = 0; i < games.length; i++) {
        if (gameIsPlaying(games[i])) {
          let gameDuration = calculateDifference(games[i]['gameStart']);
          promises.push(
            updateGame.updateDurationOfActualGame(games[i]["gameToken"], gameDuration)
          );
        }
      }
      return Promise.all(promises);
    })
    .then(()     => next())
    .catch((err) => next(err));
}

function calculateDifference (gameStart) {
  let Difference = new Date()-gameStart;
  return Difference;
}

function gameIsPlaying(game) {
  return (game['state'] == GAME_STATE.PLAYING);
}

exports.calculate = calculate;