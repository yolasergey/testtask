function catch404 (req, res, next) {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
}

function sendError (err, req, res, next) {
  res.send( {"status": "error", "code": err.status, "message": err.message});
}

exports.catch404 = catch404;
exports.sendError = sendError;