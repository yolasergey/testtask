const GAME_STATE = require('../libs/gameStates.json');
const findGame   = require('../services/findGame');
const removeGame = require('../services/removeGame');

function checkActivity (req, res, next) {
  return findGame.findAllGames({})
    .then((games) => {
      let promises = [];
      for (let i = 0; i < games.length; i++) {
        if (gameNotActual(games[i])) {
          promises.push(
            removeGame.removeNotActualGame(games[i])
          );
        }
      }
      return Promise.all(promises);
    })
    .then(()     => next())
    .catch((err) => next(err));
}

function calculateDifference (gameStart) {
  let difference = new Date()-gameStart;
  return difference;
}

function gameNotActual(game) {
  const MAXTIME = 300000;
  let timeDifference = calculateDifference(game['LastActionTime']);
  return (timeDifference>MAXTIME && game['state']!=GAME_STATE.DONE);
}

exports.checkActivity = checkActivity;