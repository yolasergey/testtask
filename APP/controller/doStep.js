const findGame    = require('../services/findGame');
const updateGame  = require('../services/updateGame');
const checkWinner = require('../services/checkWinner');

const RESPONSE    = require('../libs/responseCodes.json');
const MESSAGE     = require('../libs/errorMessages.json');
const GAME_STATE  = require('../libs/gameStates.json');
const GAME_MARKS  = require('../libs/gameMarks.json');

function make(req) {
  const gameToken   = req.header("gameToken");
  const accessToken = req.header("accessToken");
  const stepRow     = parseInt(req.body.row, 10) - 1;
  const stepCol     = parseInt(req.body.col, 10) - 1;

  return verifyAccess(gameToken, accessToken)
    .then(()     => verifyGame(gameToken, accessToken))
    .then((game) => verifyGameStep(game, accessToken, stepRow, stepCol))
    .then((game) => verifyFieldIsEmpty(game, stepRow, stepCol))
    .then((game) => {
      let turnLetter = detectNextTurnLetter(game, accessToken);
      let nextTurn   = detectNextTurnPlayer(game, accessToken);
      let fieldArr   = updateField(game, stepRow, stepCol, turnLetter);
      let winner     = checkWinner.docheck(fieldArr);   

      if (winner == GAME_MARKS.UNDEFINED) {
        if (noStepsToDo(game)) {
          return updateGame.setStatusDraw(gameToken, fieldArr);
        } 
        else {
          return updateGame.setStatusPlaying(gameToken, fieldArr, nextTurn);
        }
      } 
      else {
        if (winner == GAME_MARKS.CROSS) {
          let winnerName = game['owner'];
          let gameResult = "owner";
          return updateGame.setStatusWin(gameToken, fieldArr, gameResult, winnerName);
        } 
        else {
          let winnerName = game['opponent'];
          let gameResult = "opponent";
          return updateGame.setStatusWin(gameToken, fieldArr, gameResult, winnerName);
        }
      }
    });
}

function verifyGame(gameToken, accessToken) {
  return findGame.findGameForPlayer(accessToken, gameToken)
    .then((game) => {
      return new Promise((resolve, reject) => {
        if (game) {
          resolve(game);
        }
        else {
          reject({ "status": RESPONSE.AUTHORIZATION_ERROR, "message": MESSAGE.AUTH_FAILED_GAME_TOKEN_OR_ACCESS_TOKEN });
        }
      });
    });
}

function verifyAccess(gameToken, accessToken) {
  return new Promise((resolve, reject) => {
    if (accessToken && gameToken) {
      resolve();
    }
    else {
      reject({ "status": RESPONSE.AUTHORIZATION_ERROR, "message": MESSAGE.AUTH_MISSED_GAME_TOKEN_OR_ACCESS_TOKEN });
    }
  });
}

function verifyGameStep(game, accessToken, stepRow, stepCol) {
  return new Promise((resolve, reject) => {
    if (!gameSizeIsValid(stepRow, stepCol)) {
      reject({ "status": RESPONSE.INCOMINGDATA_ERROR, "message": MESSAGE.WRONG_ROW_COL });
    }
    else if (!gameStateIsPlaying(game)) {
      if (game['state']==GAME_STATE.DONE) {
        reject({ "status": RESPONSE.GAMEPLAY_ERROR, "message": MESSAGE.GAME_FINISHEDGAME_TURN });
      }
      if (game['state']==GAME_STATE.READY) {
        reject({ "status": RESPONSE.GAMEPLAY_ERROR, "message": MESSAGE.GAME_NOOPP_TURN });
      }
    }
    else if (!myTurnIsTrue(game, accessToken)) {
      reject({ "status": RESPONSE.GAMEPLAY_ERROR, "message": MESSAGE.GAME_WRONGTURN });
    }
    else {
      resolve(game);
    }
  });
}

function verifyFieldIsEmpty(game, stepRow, stepCol) {
  return new Promise((resolve, reject) => {
    if (game['field'][stepRow][stepCol] == GAME_MARKS.UNDEFINED) {
      resolve(game);
    }
    else {
      reject({ "status": RESPONSE.GAMEPLAY_ERROR, "message": MESSAGE.GAME_WRONGSTEP });
    }
  });
}

function updateField(game, stepRow, stepCol, turnLetter) {
  let fieldArr      = game['field'];
  let row           = game['field'][stepRow];
  let newRow        = row.split("");
  newRow[stepCol]   = turnLetter;
  newRow            = newRow.join("");
  fieldArr[stepRow] = newRow;
  return fieldArr;
}

function detectNextTurnLetter(game, accessToken) {
  return IAmOwner(game, accessToken) ? GAME_MARKS.CROSS : GAME_MARKS.ZERO;
}

function detectNextTurnPlayer(game, accessToken) {
  return IAmOwner(game, accessToken) ? game['opponentAccessToken'] : game['ownerAccessToken']; 
}

function noStepsToDo (game) {
  const MaxStepsToDo = 8;
  return game['madeStepsNumber'] == MaxStepsToDo;
}

function IAmOwner (game, accessToken) {
  return game['ownerAccessToken'] == accessToken;
}

function myTurnIsTrue(game, accessToken) {
  return game['nextTurn'] == accessToken;
}

function gameStateIsPlaying(game) {
  return game['state'] == GAME_STATE.PLAYING;
}

function gameSizeIsValid(stepRow, stepCol) {
  const maxValue = 2;
  const minValue = 0;
  return stepRow<=maxValue && stepCol<=maxValue && stepRow>=minValue && stepCol>=minValue;
}

exports.make = make;