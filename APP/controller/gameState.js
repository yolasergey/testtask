const findGame   = require('../services/findGame');
const RESPONSE   = require('../libs/responseCodes.json');
const MESSAGE    = require('../libs/errorMessages.json');
const GAME_STATE = require('../libs/gameStates.json');

exports.check = (req) => {
  const accessToken = req.header("accessToken");
  const gameToken   = req.header("gameToken");
  let youTurn       = false;
  
  return verifyAccess(gameToken)
    .then(() => {
      if (accessToken) {
        return gameSearchForPlayMode(gameToken, accessToken, youTurn);
      } 
      else {
        return gameSearchForViewMode(gameToken);
      }
    });
};

function gameSearchForViewMode (gameToken) {
  return findGame.findGameByGameToken(gameToken)
    .then((game) => {
      return new Promise((resolve, reject) => {
        if (game) {
          let answer = makeStateResponse(game, false);
          resolve(answer);    
        }
        else {
          reject({"status": RESPONSE.AUTHORIZATION_ERROR, "message": MESSAGE.AUTH_FAILED_GAME_TOKEN });
        }
      });
    });
}

function makeStateResponse(game, turn) {
  return {
    "youTurn"      : turn, 
    "gameDuration" : game['gameDuration'], 
    "field"        : game['field'], 
    "winner"       : game['winnerName']
  };   
}

function gameSearchForPlayMode(gameToken, accessToken, youTurn) {
  return findGame.findGameForPlayer(accessToken, gameToken)
    .then((game) => {
      return new Promise((resolve, reject) => {
        if (game) {
          youTurn    = isYourTurn(game, accessToken);
          let answer = makeStateResponse(game, youTurn);
          resolve(answer);
        }
        else {
          reject({"status": RESPONSE.AUTHORIZATION_ERROR, "message": MESSAGE.AUTH_FAILED_GAME_TOKEN_OR_ACCESS_TOKEN });
        }
      });
    });
}

function verifyAccess(gameToken) {
  return new Promise((resolve, reject) => {
    if (gameToken) {
      resolve();
    } 
    else {
      reject({"status": RESPONSE.AUTHORIZATION_ERROR, "message": MESSAGE.AUTH_MISSED_GAME_TOKEN });
    }
  });
}

function isYourTurn (game, accessToken) {
  return accessToken==game['nextTurn'] && game['state']==GAME_STATE.PLAYING;
}