const randomString = require("randomstring");
const RESPONSE     = require('../libs/responseCodes.json');
const MESSAGE      = require('../libs/errorMessages.json');
const findGame     = require('../services/findGame');
const saveGame     = require('../services/saveGame');

exports.create = (req) => {
  let headerAccessToken = req.header("accessToken");
  const userName        = req.body.userName;

  if (headerAccessToken) {
    return verifyIncomingData(userName)
      .then(() => verifyGame(headerAccessToken))
      .then(() => {
        let accessToken = headerAccessToken;
        let gameToken   = randomString.generate({length: 6, charset: 'hex'});
        return saveGame.doSave(accessToken, gameToken, userName);
      });
  }
  else {
    let accessToken = randomString.generate({length: 12, charset: 'hex'});
    let gameToken   = randomString.generate({length: 6, charset: 'hex'});
    return verifyIncomingData(userName)
      .then(() => saveGame.doSave(accessToken, gameToken, userName));
  }
};

function verifyIncomingData (userName) {
  return new Promise((resolve, reject) => {
    if (userName) {
      resolve();
    }
    else {
      reject({"status": RESPONSE.INCOMINGDATA_ERROR, "message": MESSAGE.MISSED_USERNAME });
    }
  });
}

function verifyGame(headerAccessToken) {
  return findGame.findGameByAccessToken(headerAccessToken)
    .then((game) => {
      return new Promise((resolve, reject) => {
        if (game) {
          resolve();
        }
        else {
          reject({ "status": RESPONSE.AUTHORIZATION_ERROR, "message": MESSAGE.AUTH_FAILED_ACCESS_TOKEN });
        }
      });
    });
}