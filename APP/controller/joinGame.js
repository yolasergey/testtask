const updateGame   = require('../services/updateGame');
const findGame     = require('../services/findGame');
const randomString = require('randomstring');
const RESPONSE     = require('../libs/responseCodes.json');
const MESSAGE      = require('../libs/errorMessages.json');
const GAME_STATE   = require('../libs/gameStates.json');

exports.create = (req) => {
  let headerAccessToken = req.header("accessToken");
  let gameToken         = req.body.gameToken;
  let userName          = req.body.userName;
  
  if (!headerAccessToken) {
    let opponentAccessToken = randomString.generate({length: 12, charset: 'hex'});
    return joinGame(gameToken, userName, opponentAccessToken);
  } 
  else {
    return findGame.findGameByAccessToken(headerAccessToken)
      .then((game)                => tokenIsCorrect(game, headerAccessToken))
      .then((opponentAccessToken) => joinGame(gameToken, userName, opponentAccessToken));
  }
}; 

function tokenIsCorrect(game, headerAccessToken) {
  return new Promise((resolve, reject) => {
    if (game) {
      let opponentAccessToken = headerAccessToken;
      resolve(opponentAccessToken);
    }
    else {
      reject ({"status": RESPONSE.AUTHORIZATION_ERROR, "message": MESSAGE.AUTH_FAILED_ACCESS_TOKEN });
    }
  });
}

function joinGame(gameToken, userName, opponentAccessToken) {
  return verifyAccess(gameToken, userName)
    .then(()     => findGame.findGameByGameToken(gameToken))
    .then((game) => gameIsReady(game))
    .then((game) => tokenIsNotSame(game, opponentAccessToken))
    .then(()     => updateGame.updateJoinedGame(gameToken, userName, opponentAccessToken));
}

function tokenIsNotSame(game, opponentAccessToken) {
  return new Promise((resolve, reject) => {
    if (game['ownerAccessToken'] != opponentAccessToken) {
        resolve();
    }
    else {
      reject ({"status": RESPONSE.INCOMINGDATA_ERROR, "message": MESSAGE.SAME_TOKEN });
    }
  });
}

function gameIsReady(game) {
  return new Promise((resolve, reject) => {
    if (game && game['state']==GAME_STATE.READY) {
        resolve(game);
    }
    else if (!game) {
      reject ({"status": RESPONSE.AUTHORIZATION_ERROR, "message": MESSAGE.AUTH_FAILED_GAME_TOKEN });
    }
    else {
      if (game['state']==GAME_STATE.PLAYING) {
        reject ({"code": RESPONSE.JOINGAME_ERROR, "message": MESSAGE.GAME_STARTED });
      }
      if (game['state']==GAME_STATE.DONE) {
        reject ({"code": RESPONSE.JOINGAME_ERROR, "message": MESSAGE.GAME_FINISHED });
      }
    }
  });
}

function verifyAccess(gameToken, userName) {
  return new Promise((resolve, reject) => {
    if (gameToken && userName) {
      resolve();
    }
    else {
      reject({ "status": RESPONSE.AUTHORIZATION_ERROR, "message": MESSAGE.AUTH_FAILED_GAME_TOKEN_OR_USER_NAME });
    }
  });
}