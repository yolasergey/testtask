const findGame = require('../services/findGame');

exports.create = () => {
  let searchAttribute = {
    '_id'         : 0, 
    'gameToken'   : 1, 
    'owner'       : 1, 
    'opponent'    : 1, 
    'size'        : 1, 
    'gameDuration': 1, 
    'gameResult'  : 1, 
    'state'       : 1
  };
  return findGame.findAllGames(searchAttribute)
    .then((games) => makeAnswer(games));
};

function makeAnswer (games) {
  return new Promise((resolve, reject) => {
      resolve({"games": games});
  });
}