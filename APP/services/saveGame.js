const Game     = require('../model/game').game;
const RESPONSE = require('../libs/responseCodes.json');
const MESSAGE  = require('../libs/errorMessages.json');

function doSave (accessToken, gameToken, userName) {
  let game = new Game({
    gameToken        : gameToken,
    owner            : userName,
    ownerAccessToken : accessToken,
    nextTurn         : accessToken,
    LastActionTime   : new Date(),
  });
  
  return new Promise((resolve, reject) => {
    game.save((err) => {
      if(!err) {
        resolve({"accessToken": accessToken, "gameToken": gameToken});
      }
      else {
        reject({ "status": RESPONSE.DATABASE_ERROR, "message": MESSAGE.DB_SAVE });
      }
    });
  });
}

exports.doSave = doSave;