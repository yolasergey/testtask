const Game       = require('../model/game').game;
const RESPONSE   = require('../libs/responseCodes.json');
const MESSAGE    = require('../libs/errorMessages.json');
const GAME_STATE = require('../libs/gameStates.json');

function updateDurationOfActualGame(gameToken, gameDuration) {
  let condition = {$set: {'gameDuration': gameDuration}};
  let answer    = {};
  return updateGame(gameToken, condition, answer);
}

function updateJoinedGame(gameToken, userName, opponentAccessToken) {
  let answer    = {"accessToken": opponentAccessToken};
  let condition = {
    $set: {
      "opponent"            : userName, 
      "opponentAccessToken" : opponentAccessToken,
      "state"               : GAME_STATE.PLAYING, 
      "gameStart"           : new Date(), 
      "LastActionTime"      : new Date()
    }
  };
  return updateGame(gameToken, condition, answer);
}

function setStatusPlaying(gameToken, fieldArray, nextTurn) {
  let condition = {
    $set: {
      'field'              : fieldArray, 
      'LastActionTime'     : new Date(), 
      'nextTurn'           : nextTurn}, 
    $inc: {'madeStepsNumber' : 1}
  };
  let answer = {};
  return updateGame(gameToken, condition, answer);
}

function setStatusWin(gameToken, fieldArray, gameResult, winnerName) {
  let answer    = { "gameResult": gameResult };
  let condition = {
    $set: {
      'field'             : fieldArray, 
      'LastActionTime'    : new Date(), 
      'state'             : GAME_STATE.DONE, 
      'winnerName'        : winnerName, 
      'nextTurn'          : 'nobody', 
      'gameResult'        : gameResult}, 
    $inc: {'madeStepsNumber': 1}
  };
  return updateGame(gameToken, condition, answer);
}

function setStatusDraw(gameToken, fieldArray) {
  let answer    = { "gameResult": "draw" };
  let condition = {
    $set: {
      'field'              : fieldArray, 
      'LastActionTime'     : new Date(), 
      'state'              : GAME_STATE.DONE, 
      'winnerName'         : "Draw. We don't have a winner", 
      'nextTurn'           : 'nobody', 
      'gameResult'         : "draw"}, 
    $inc: {'madeStepsNumber' : 1}
  };
  return updateGame(gameToken, condition, answer);
}

function updateGame (gameToken, condition, answer) {
  return new Promise((resolve, reject) => {
    Game.update({"gameToken": gameToken}, condition, {'_id':0}, (err) => {
      if(!err) {
        resolve(answer);
      }
      else {
        reject({ "status": RESPONSE.DATABASE_ERROR, "message": MESSAGE.DB_UPDATE });
      }
    });
  });
}

exports.setStatusPlaying           = setStatusPlaying;
exports.setStatusWin               = setStatusWin;
exports.setStatusDraw              = setStatusDraw;
exports.updateDurationOfActualGame = updateDurationOfActualGame;
exports.updateJoinedGame           = updateJoinedGame;
