const conf = require('../config/nconf');
let db     = require('mongoose');
db.Promise = global.Promise;
db.connect(conf.get('dbConnection'), {useMongoClient: true,});

module.exports = db;