const RESPONSE = require('../libs/responseCodes.json');
const STATUS   = require('../libs/statuses.json');

function addSuccessStatus(response) {
  response["status"] = STATUS.OK;
  response["code"]   = RESPONSE.SUCCESS;
  return response;
}

exports.addSuccessStatus = addSuccessStatus;