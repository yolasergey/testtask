const GAME_MARKS = require('../libs/gameMarks.json');

exports.docheck = (field) => {
  let newArray  = makeNewArray(field);
  let winner    = compareArrays(newArray);
  return winner;
};

function makeNewArray(fieldArray) {
  let singleArray=[];
  for (let i = 0; i < fieldArray.length; i++) {
    for (let y = 0; y<3; y++) {
      let element = fieldArray[i][y];
      singleArray.push(element);
    }
  }
  return singleArray;
}

function compareArrays(arr) {
  let win = GAME_MARKS.UNDEFINED;
  let winnerMatrix = [
    [0, 1, 2], 
    [3, 4, 5], 
    [6, 7, 8], 
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]];
      
  for(let i = 0; i < 8; i++) {
    if (winnerIsTrue(arr, winnerMatrix, i)) {
      win = arr[winnerMatrix[i][0]]; 
      break; 
    } 
  } 
  return win;
}

function winnerIsTrue (arr, winnerMatrix, i) {
  return (arr[winnerMatrix[i][0]] == arr[winnerMatrix[i][1]] &&
          arr[winnerMatrix[i][1]] == arr[winnerMatrix[i][2]] && 
          arr[winnerMatrix[i][0]] != GAME_MARKS.UNDEFINED);
}