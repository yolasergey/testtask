const Game     = require('../model/game').game;
const RESPONSE = require('../libs/responseCodes.json');
const MESSAGE  = require('../libs/errorMessages.json');

function removeNotActualGame(game) {
  return new Promise((resolve, reject) => {
    Game.remove({"gameToken": game["gameToken"]}, (err) => {
      if(!err) {
        resolve();
      }
      else {
        reject({ "status": RESPONSE.DATABASE_ERROR, "message": MESSAGE.DB_REMOVE });
      }
    });
  });
}

exports.removeNotActualGame = removeNotActualGame;