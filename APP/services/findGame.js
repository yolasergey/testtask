const Game     = require('../model/game').game;
const RESPONSE = require('../libs/responseCodes.json');
const MESSAGE  = require('../libs/errorMessages.json');

function doSearch (searchCondition) {
  return new Promise((resolve, reject) => {
    Game.findOne(searchCondition, (err, game) => {
      if(!err) {
        resolve(game);
      }
      else {
        reject({ "status": RESPONSE.DATABASE_ERROR, "message": MESSAGE.DB_SEARCH });
      }
    });
  });
}

function findAllGames (searchAttribute) {
  return new Promise((resolve, reject) => {
    Game.find({}, searchAttribute, (err, games) => {
      if(!err) {
        resolve(games);
      } 
      else {
        reject({ "status": RESPONSE.DATABASE_ERROR, "message": MESSAGE.DB_SEARCH });
      } 
    });
  });
}

function findGameByAccessToken(headerAccessToken) {
  let searchCondition = {$or: [{"ownerAccessToken": headerAccessToken}, {"opponentAccessToken": headerAccessToken}]};
  return doSearch(searchCondition);
}

function findGameByGameToken(gameToken) {
  let searchCondition = {"gameToken": gameToken};
  return doSearch(searchCondition);
}

function findGameForPlayer(accessToken, gameToken) {
  let searchCondition = {
    "gameToken": gameToken, 
    $or: [
      {"ownerAccessToken": accessToken}, 
      {"opponentAccessToken": accessToken}
    ]
  };
  return doSearch(searchCondition);
}

exports.findGameForPlayer     = findGameForPlayer;
exports.findGameByGameToken   = findGameByGameToken;
exports.findGameByAccessToken = findGameByAccessToken;
exports.findAllGames          = findAllGames;