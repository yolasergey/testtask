const express        = require('express');
const router         = express.Router();
const ResponseStatus = require('../services/ResponseStatus');

const newGame        = require('../controller/newGame');
const joinGame       = require('../controller/joinGame');
const gameState      = require('../controller/gameState');
const doStep         = require('../controller/doStep');
const gamesList      = require('../controller/gamesList');

router.post('/games/new', (req, res, next) => {
  return newGame.create(req, res)
    .then((responseObject) => res.send(ResponseStatus.addSuccessStatus(responseObject)))
    .catch((err)           => next(err));
});

router.get('/games/list', (req, res, next) => {
  return gamesList.create(req, res)
    .then((responseObject) => res.send(ResponseStatus.addSuccessStatus(responseObject)))
    .catch((err)           => next(err));
});

router.post('/games/join', (req, res, next) => {
  return joinGame.create(req, res)
    .then((responseObject) => res.send(ResponseStatus.addSuccessStatus(responseObject)))
    .catch((err)           => next(err));
});

router.post('/games/do_step', (req, res, next) => {
  return doStep.make(req, res)
    .then((responseObject) => res.send(ResponseStatus.addSuccessStatus(responseObject)))
    .catch((err)           => next(err));
});

router.get('/games/state', (req, res, next) => {
  return gameState.check(req, res)
    .then((responseObject) => res.send(ResponseStatus.addSuccessStatus(responseObject)))
    .catch((err)           => next(err));
});

module.exports = router;
