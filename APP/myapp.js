const express = require('express');
const app     = express();

const conf = require('./config/nconf');
const port = conf.get('port');

const index = require('./routes/index');

const timer         = require('./middlewares/activityTimer');
const duration      = require('./middlewares/durationCalculator');
const errorsHandler = require('./middlewares/errorsHandler');
const log           = require('./middlewares/logger');

const fs = require('fs');
let path = require('path');
let accessLogStream = fs.createWriteStream(path.join(__dirname, 'myapp.log'), {flags: 'a'});

const bodyParser = require('body-parser');
const http       = require('http');

app.use(log(conf.get('logLevel'), {stream: accessLogStream}));
app.use(bodyParser.json());
app.use(duration.calculate);
app.use(timer.checkActivity);
app.use('/', index);
app.use(errorsHandler.catch404);
app.use(errorsHandler.sendError);

// eslint-disable-next-line no-console
http.createServer(app).listen(port, () => console.log(`Listening on port ${port}`));