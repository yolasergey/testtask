let db = require('../services/dbconnect');
let schemaGame = new db.Schema({
  gameToken: {
    type: String,
    require: true,
    unique: true
  },
  owner: {
    type: String,
    require: true,
  },
   ownerAccessToken: {
    type: String,
    require: true,
  },
  opponent: {
    type: String,
  },
  opponentAccessToken: {
    type: String,
  },
  size: {
    type: Number,
    default: 3,
  },
  gameStart: {
    type: Date,
  },
  LastActionTime: {
    type: Date,
  },
  gameDuration: {
    type: Number,
    default: 0,
  },
  gameResult: {
    type: String,
  },
  winnerName: {
    type: String,
    default: '',
  },
  state: {
    type: String,
    default: "ready",
  },
  nextTurn: {
    type: String,
    default: "",
  },
  madeStepsNumber: {
    type: Number,
    default: 0,
  },
  field: {
    type: Array,
    default: ["???", "???", "???"],
  },
});

exports.game = db.model('game', schemaGame);