Формат запросов для работы с приложением.

1) Действие: Создание новой партии
    URL: /games/new
    Метод: POST
    Заголовки:
        Content-type: application/json
        accessToken: <access token пользователя, если имеется>
    Пример тела запроса:
      {"userName": "Ivan Ivanov"}
    Размер поля указывать не требуется, он устанавливается по умолчанию 3x3.
    Дополнительное задание в виде игры других размеров не выполнялось.
      
2) Действие: Получение списка партий
    URL: /games/list
    Метод: GET

3) Присоединение к существующей партии
    URL:/games/join
    Метод: POST
    Заголовки:
        Content-type: application/json
        accessToken: <access token пользователя, если имеется>
    Пример тела запроса:
      {"gameToken":"8e6f53", "userName": "Petya Petrov"}
        
4) Осуществление хода
    URL:/games/do_step
    Метод: POST
    Заголовки:
        Content-type: application/json
        accessToken: <обязателен>
        gameToken: <обязателен>
    Пример тела запроса:
      {"row": 3, "col": 1}
      
5) Получение текущего состояния
    URL:/games/state
    Метод: GET
    Заголовки:
        Content-type: application/json
        gameToken: <обязателен>
        accessToken: <обязателен для игрока в режиме "игра"; не обязателен для зрителя в режиме "просмотр">
      
